import Imgprofile from '../assets/img/1.jpeg'
import Imgprofile2 from '../assets/img/2.jpeg'
import Imgprofile3 from '../assets/img/3.jpeg'
export default {
    name: "Fateme Moradi",
    home: "HOME",
    about: "ABOUT ME",
    resume: "RESUME",
    contact: "CONTACT",
    portfolies: "PORTFOLIOS",
    hi: "Hi, I'm ",
    hi2: "",
    desc: "I am frontend web developer.I can provide clean code and pixel perfect design.I also make website more & more interactive with web animations.",
    FullName: "Full Name",
    Age: "Age",
    Nationality: "Nationality",
    Languages: "Languages",
    Phone: "Phone",
    iranian: "iranian",
    persian: "persian",
    english: "english",
    downloadcv: "Download CV",
    services:'SERVICES',
    experience :[
        { id: -1, date: '  ', title: ' ', subtitle: ' ', desc: ' ' },
        { id: 0, date: '2018-2019', title: 'experience', subtitle: 'sub', desc: 'deschdhtfthfyhjyfruy' },
        { id: 1, date: '2018-2019', title: 'experience', subtitle: 'sub', desc: 'descvgvhnvjhfjy' },
        { id: -2, date: '  ', title: ' ', subtitle: ' ', desc: ' ' },
      ],
    study :[
        { id: -1, date: '  ', title: ' ', subtitle: ' ', desc: ' ' },
        { id: 0, date: '2016-2017', title: 'Ershad', subtitle: 'sub', desc: 'deschdhtfthfyhjyfruy' },
        { id: 1, date: '2018-2019', title: 'taleghani', subtitle: 'sub', desc: 'descvgvhnvjhfjy' },
        { id: -2, date: '  ', title: ' ', subtitle: ' ', desc: ' ' },
      ],
      image:[
        {id:0,image:Imgprofile2,title:'title1',desc:'desc1'},
        {id:1,image:Imgprofile3,title:'title2',desc:'desc2'},
        {id:2,image:Imgprofile,title:'title3',desc:'desc3'},
        {id:3,image:Imgprofile2,title:'title3',desc:'desc3'},
        {id:4,image:Imgprofile3,title:'title3',desc:'desc3'},
        {id:5,image:Imgprofile,title:'title4',desc:'desc4'},
        ] 
}