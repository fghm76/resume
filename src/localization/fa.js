import Imgprofile from '../assets/img/1.jpeg'
import Imgprofile2 from '../assets/img/2.jpeg'
import Imgprofile3 from '../assets/img/3.jpeg'
export default{
    name:" فاطمه مرادی",
    home:"خانه",
    about:"درباره من",
    resume:"رزومه",
    contact:"ارتباط با ما",
    portfolies:"نمونه کارها",
    hi:" سلام من ",
    hi2:" هستم ",
    desc:'من یک فرانت وب دولوپر هستم. من میتوانم کد های بهینه و طراحی پیکسل بی نقصی داشته باشم. همینطور وب سایتهایی جذابی توسط انیمیشن ها میسازم.',
    FullName: "نام کامل",
    Age: "سن",
    Nationality: "ملیت",
    Languages: "زبان ها",
    Phone: "تلفن",
    iranian: "ایرانی",
    persian: "فارسی",
    english: "انگلیسی",
    downloadcv: "دانلود رزومه",
    services:'سرویس ها',
    experience :[
        { id: -1, date: '  ', title: ' ', subtitle: ' ', desc: ' ' },
        { id: 0, date: '2018-2019', title: 'experience', subtitle: 'sub', desc: 'deschdhtfthfyhjyfruy' },
        { id: 1, date: '2018-2019', title: 'experience', subtitle: 'sub', desc: 'descvgvhnvjhfjy' },
        { id: -2, date: '  ', title: ' ', subtitle: ' ', desc: ' ' },
      ],
    study :[
        { id: -1, date: '  ', title: ' ', subtitle: ' ', desc: ' ' },
        { id: 0, date: '2016-2017', title: 'Ershad', subtitle: 'sub', desc: 'deschdhtfthfyhjyfruy' },
        { id: 1, date: '2018-2019', title: 'taleghani', subtitle: 'sub', desc: 'descvgvhnvjhfjy' },
        { id: -2, date: '  ', title: ' ', subtitle: ' ', desc: ' ' },
      ],
      image:[
        {id:0,image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpev7Ee3lA6t8aHF1WJ_SCAV1ksCBUrsxhtizOgm6v_0vUCI6JATxiradpKnFtMQ7O4vo&usqp=CAU',title:'عنوان',desc:'توضیحات'},
        {id:1,image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjMIQqyZDL6-uUS2CNp37_x9Se4QSeOB8Gltsoxc4w5BO8TSx38NYJbQaUr5k8Xn2_bWc&usqp=CAU',title:'عنوان',desc:'توضیحات'},
        {id:2,image:'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTcZ8OxwIpBaPGIMpUeSoWebPz-EH0XrPGzcQ&usqp=CAU',title:'عنوان',desc:'توضیحات'},
        {id:3,image:Imgprofile2,title:'title3',desc:'desc3'},
        {id:4,image:Imgprofile3,title:'title3',desc:'desc3'},
        {id:5,image:Imgprofile,title:'title4',desc:'desc4'},
              ] 
}